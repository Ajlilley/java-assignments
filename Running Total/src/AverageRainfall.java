import java.util.Scanner;
public class AverageRainfall {

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		int years;
		int months = 0;
		double rainfall = 0;
		double averageRain;
		double total = 0;
		
		System.out.println("Enter number of years?");
		years = keyboard.nextInt();
		while (years < 1)
		{
			System.out.println("the number of years you enter must be 1 or greater");
			System.out.println("Enter number of years?");
			years = keyboard.nextInt();
		}
		
		for (int x = 1; x <= years; x++)
		{
			System.out.println("YEAR: " + x);
			months = 1;
			while (months <= 12)
			{
				System.out.println("Month: " + months);
				System.out.println("How many inches of rain were there for this month?");
				rainfall = keyboard.nextDouble();
				
				while (rainfall < 0)
				{
					System.out.println("Rainfall measurement must be positive");
					System.out.println("How many inches of rain were there for this month?");
					rainfall = keyboard.nextDouble();
				}
				
				total += rainfall;	
				months++;
			}			
		}
		
		months = years * 12;
		averageRain = total / months;
		System.out.printf("Over %d months, there was an average of %,.2f inches of rain.", months, averageRain);

	}

}