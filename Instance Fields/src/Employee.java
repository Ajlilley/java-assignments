
public class Employee 
{
		  
  private String name;
  private String employeeNumber;
  private String hireDate;

  public Employee(String n, String num, String date) 
  {
    name = new String(n);
    employeeNumber = new String(num);
    hireDate = new String(date);
  }
  
  public void setName(String n) 
  {
    name = n;
  }
  
  public void setEmployeeNumber(String e) 
  {
    employeeNumber = e;
  }
  
  public void setHireDate(String h)
  {
    hireDate = h;
  }

  public String getName() 
  {
    return name;
  }

  public String getEmployeeNumber() 
  {
    return employeeNumber;
  }

  public String getHireDate() 
  {
    return hireDate;
  }
  
  public boolean isValidEmpNum(String e)
  {
  boolean status = true;

  	if (e.length() != 5)
  		status = false;
  	else
  	{
  	if ((!Character.isDigit(e.charAt(0))) || !Character.isDigit(e.charAt(1)) || !Character.isDigit(e.charAt(2)) || (e.charAt(3) != '-') || (!Character.isLetter(e.charAt(4))))
  		status = false;
  	}
  	
  	return status;
  }

  public String toString() 
  {
    String employeeString = "Employee Name: " + name +
                 "\nEmployee Number: " + employeeNumber +
                 "\nEmployee Hire Date: " + hireDate;
    return employeeString;
  }
}
