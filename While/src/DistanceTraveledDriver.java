import java.util.Scanner;

public class DistanceTraveledDriver 
{

	public static void main(String[] args) 
	{
		double speed = 0;
		double time = 0;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the mph at which the vehicle is traveling: ");
		speed = sc.nextDouble();
		
		System.out.println("Enter the number of hours at which the vehicle traveled: ");
		time = sc.nextDouble();
		
		
		
		while (time < 1 || speed < 1)
		{
			System.out.println("Invalid number! Please enter a positive number.");
			
			System.out.println();
			
			System.out.println("Enter the mph at which the vehicle is traveling: ");
			speed = sc.nextDouble();
			
			System.out.println("Enter the number of hours at which the vehicle traveled: ");
			time = sc.nextDouble();
		}
		
		DistanceTraveled distanceTraveled = new DistanceTraveled(speed, time);
		
		double remain = 0;
		
		if(time % 1 != 0)
		{
			remain = time % 1;
			time = (int)time / 1;
			
		}
		
		double i;
		
		System.out.println("Hour                       Distance" +
				   "\n____________________________________");
		
		for (i = 0; i < time ; ++i)
		{	
			distanceTraveled.setTime((i+1));
			System.out.printf("%.2f", i+1);
			System.out.printf("%30.2f\n",distanceTraveled.calcDistance());
		}
		
		if(remain != 0)
		{
			distanceTraveled.setTime((i + remain));
			System.out.printf("%.2f", (i + remain));
			System.out.printf("%30.2f\n",distanceTraveled.calcDistance());
		}
		
	}

}

