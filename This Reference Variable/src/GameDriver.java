public class GameDriver 
{
	public static void main(String[] args) 
	{
	 //base game
    Game newGame = new Game("Tetris", 2);

    //print base game
    System.out.println("\nBase Game is: ");
    System.out.println(newGame);

    //create timed game
    GameWithTimeLimit newGameTimed = new GameWithTimeLimit("Pacman", 2, 15);

    //print timed game
    System.out.println("\nThe Timed Game is: ");
    System.out.println(newGameTimed);
	}
}
