public class GameWithTimeLimit extends Game
{
	//variables
	private int timeLimit;
	
	
	//Constructor
	public GameWithTimeLimit(String game, int numPlayers, int timeLimit) 
	{
		super(game, numPlayers);
		this.timeLimit = timeLimit;
	}
	
	//TimeLimit getter
	public int getTimeLimit() 
	{
		return timeLimit;
	}
	
	//TimeLimit Setter
	public void setTimeLimit(int timeLimit) 
	{
		this.timeLimit = timeLimit;
	}
	
	//ToString method
	public String toString() 
	  {
	    String str = "Game: " + game +
                	"\nNumber of players: " + numPlayers +
                	"\nTime Limit: " + timeLimit + "minutes";
	    return str;
	  }
}
