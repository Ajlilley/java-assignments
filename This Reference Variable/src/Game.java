public class Game 
{
	//variables
	String game;
	int numPlayers = 2;
	
	//constructor
	public Game(String game, int numPlayers) 
	{
		this.game = game;
		this.numPlayers = numPlayers;
	}
	
	//game getter
	public String getGame() 
	{
		return game;
	}
	
	//game setter
	public void setGame(String game) 
	{
		this.game = game;
	}
	
	//numPlayers getter
	public int getNumPlayers() 
	{
		return numPlayers;
	}
	
	//numPlayers setter
	public void setNumPlayers(int numPlayers) 
	{
		this.numPlayers = numPlayers;
	}
	
	//ToString method
	public String toString() 
	  {
	    String str = "Game: " + game +
	                 "\nNumber of players: " + numPlayers;
	    return str;
	  }
}
