import java.util.Scanner;

public class SoccerPoints 
{
	public static void main(String[] args)
	{
		int points;
		int totalPoints = 0;
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter number of points your team has earned each game or -1 to end");
		points = keyboard.nextInt();
		
		//Acumulate until -1 is entered
		while (points !=-1)
		{
			totalPoints += points;
			System.out.print("Enter number of points or -1 to end");
			points = keyboard.nextInt();
		}
		
		System.out.println("Your team earned a total of " + totalPoints + " points.");
	}
}
