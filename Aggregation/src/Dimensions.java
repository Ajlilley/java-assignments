public class Dimensions 
{
	private double width;
	private double length;
	
	public Dimensions(double width, double length)
	{
		this.length = length;
		this.width = width;
	}
	
	public double getArea()
	{
		return length * width;
	}
	
	public String toString()
	{
		return ("The length of the room is " + length + " feet. The width of the room is " 
				+ width + " feet. \nThe area of the room is " + getArea() + " square feet.");
	}
}

