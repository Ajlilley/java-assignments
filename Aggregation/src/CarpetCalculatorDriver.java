import java.util.Scanner;

public class CarpetCalculatorDriver 
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		
		System.out.println("What is the width of the room?");
		double width = sc.nextDouble();
		
		System.out.println("What is the length of the room?");
		double length = sc.nextDouble();
		
		Dimensions dims = new Dimensions(width, length);
		
		System.out.println("What is the cost of carpet per square foot?");
		double cost = sc.nextDouble();
		
		CarpetCalculator carpet = new CarpetCalculator(dims, cost);
		
		System.out.println(carpet.toString());
		
	}
}
