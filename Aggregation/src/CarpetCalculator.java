public class CarpetCalculator 
{
	private Dimensions size;
	private double cost;
	
	public CarpetCalculator (Dimensions dims, double cost)
	{
		this.size = dims;
		this.cost = cost;
	}
	
	public double getCostTotal()
	{
		return size.getArea() * cost;
	}
	
	public String toString()
	{
		return size.toString() + "\nThe cost per square feet is $" + cost + 
				"\nThe total cost for carpet is $" + getCostTotal();
	}
}
