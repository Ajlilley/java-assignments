import java.util.Scanner;
import java.text.DecimalFormat;
 
public class HotelOccupancy
 	{
 		public static void main(String[] args)
 		{
 			Scanner sc = new Scanner(System.in);
 			DecimalFormat formatter = new DecimalFormat("#.000");
 			int totalFloors, roomsPerFloor, roomsUsed;
 			int openRooms = 0, occupiedRooms = 0, allRooms = 0;
 			double rateOfOccupancy = 0;
 			
 			
 			//Ask for the number of floors in the hotel: numberOfFloors
 			//Number of floor mustn't be less than one. 
 			
 			System.out.println("How many floors are in the hotel?");
 			totalFloors = sc.nextInt();
 			
 			while(totalFloors < 1)
 			{
 				System.out.println("Invalid number of floors! Please try again");
 				totalFloors = sc.nextInt();
 			}
 			
 			int floors = 0;
 			
 			for(int i = 1; i <= totalFloors; i++ )	
 			{
 				
 				floors++;
 				
 				System.out.println("Floor " + floors +":");
 				System.out.println("How many rooms on this floor");
 				roomsPerFloor = sc.nextInt();
 				
 				
 				while(roomsPerFloor < 10)
 				{	
 					System.out.println("Invalid Input, 10 or greater rooms per floor");
 					System.out.println("How many rooms on this floor");
 					roomsPerFloor = sc.nextInt();
 				}
 				
 				System.out.println("How many rooms are occupied on this floor?");
 				roomsUsed = sc.nextInt();
 				allRooms += roomsPerFloor;
 				occupiedRooms += roomsUsed;
 				rateOfOccupancy = occupiedRooms/(double)(allRooms);
 				openRooms = allRooms - occupiedRooms;
 				
 			}
 			
 			System.out.println("Total Hotel Rooms: " + allRooms);
			System.out.println("Rooms Occupied: " + occupiedRooms);
 			System.out.println("Rooms Available: " + openRooms);
 			System.out.println("Occupancy Rate: " + formatter.format(rateOfOccupancy));
		
 		}
 
 }