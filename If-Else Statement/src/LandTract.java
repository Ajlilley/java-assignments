
public class LandTract 
{
	private double width;
	private double length;
	private double area;

	public LandTract()
	{
		area = 0.0;
	}
	
	public LandTract(double length, double width)
	{
		this.length = length;
		this.width = width;
	}
	
	public double getLength(double length)
	{
		return length;
	}
	
	public double getWidth(double width)
	{
		return width;
	}
	
	public double getArea()
	{
		area = length * width;
		return area;
	}
	
	public boolean equals(LandTract land2)
	{
	return this.area == land2.area;
	}
	
	public String toString()
	{
		return "" + getArea();
	}
}
