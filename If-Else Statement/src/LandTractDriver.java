
import java.util.Scanner;

public class LandTractDriver
{
    public static void main(String[] args)
    {
    	double length;          
        double width;        
         
      
        Scanner sc = new Scanner(System.in);
      
        System.out.print("Enter the width of Tract 1: ");
		width = sc.nextDouble();
        System.out.print("Enter the length of Tract 1: ");
        length = sc.nextDouble();
  
		LandTract land1 = new LandTract(length, width);
  
		System.out.print("Enter the width of tract 2: ");
		width = sc.nextDouble();
		System.out.print("Enter the length of tract 2: ");
		length = sc.nextDouble();
  
  
		LandTract land2 = new LandTract(length, width);
  
  
		System.out.println("\nTract 1 Area: " + land1);
		System.out.println("Tract 2 Area: " + land2 + "\n");
      
         if (land1.equals(land2))
        	 
            System.out.println("The tracts are the same size");
         
         else
        	 
            System.out.println("The tracts are not the same size");
      }
   }
