import java.util.Scanner;

public class MonthDriver
{
	public static void main(String[] args)
	{
	    Scanner sc = new Scanner(System.in);
	    
	    Integer numberOfMonth;
	    
	    Month monthChoice = new Month();
	
	    System.out.print("Enter a number of month: ");
	    numberOfMonth = sc.nextInt();
	    monthChoice.setMonth(numberOfMonth);
	
	    System.out.println(numberOfMonth + " is the month of " + monthChoice.toString());
	}
}
