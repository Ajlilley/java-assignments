import javafx.scene.control.Label;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.event.ActionEvent;

public class JoesAutomotive extends Application
{
	private TextField userInputTextField;
	private Label resultLabel1;
	
	private CheckBox oilCheck;
	private CheckBox lubeCheck;
	private CheckBox RadiatorCheck;
	private CheckBox TransmissionCheck;
	private CheckBox inspectionCheck;
	private CheckBox mufflerCheck;
	private CheckBox tirerotationCheck;
	
	private double oilCost = 0;
	private double lubeCost = 0;
	private double RadiatorCost = 0;
	private double TransmissionCost = 0;
	private double inspectionCost = 0;
	private double mufflerCost = 0;
	private double tirerotationCost = 0;
	private double totalCost = 0;
	
	public static void main(String[] args) throws NumberFormatException
	{
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		Label userInputLabel = new Label("Enter the hours of labor here: ");
		
		userInputTextField = new TextField();
		
		Button calcButton = new Button("Calculate");
		calcButton.setOnAction(new calcButtonHandler());
		
		resultLabel1 = new Label();
		
		oilCheck = new CheckBox("Oil Change");
		lubeCheck = new CheckBox("Lube Job");
		RadiatorCheck = new CheckBox("Radiator Flush");
		TransmissionCheck = new CheckBox("Transmission Flush");
		inspectionCheck = new CheckBox("Inspection");
		mufflerCheck = new CheckBox("Muffler replacement");
		tirerotationCheck = new CheckBox("Tire Rotation");
		
		HBox hbox1 = new HBox(10, userInputLabel, userInputTextField);
		VBox vbox = new VBox(10, hbox1, oilCheck, lubeCheck, RadiatorCheck, TransmissionCheck, 
				inspectionCheck, mufflerCheck, tirerotationCheck, calcButton, resultLabel1);
		
		hbox1.setAlignment(Pos.CENTER);
		vbox.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(vbox);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Property Tax");
		primaryStage.show();
	}
	
	class calcButtonHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			
			double userInput = Double.parseDouble(userInputTextField.getText());
			
			if(oilCheck.isSelected())
			{
				oilCost = 35;
			}
			if(lubeCheck.isSelected())
			{
				lubeCost = 25;
			}
			if(RadiatorCheck.isSelected())
			{
				RadiatorCost = 50;
			}
			if(TransmissionCheck.isSelected())
			{
				TransmissionCost = 120;
			}
			if(inspectionCheck.isSelected())
			{
				inspectionCost = 35;
			}
			if(mufflerCheck.isSelected())
			{
				mufflerCost = 200;
			}
			if(tirerotationCheck.isSelected())
			{
				tirerotationCost = 20;
			}
			
			totalCost = (oilCost + lubeCost + RadiatorCost + TransmissionCost + inspectionCost + mufflerCost 
						+ tirerotationCost + (userInput * 60));
			
			resultLabel1.setText(String.format("The cost of your service is: $%,.2f", totalCost));
		}
	}
}
