import java.util.Random;
import javafx.scene.control.Label;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class HeadsOrTails extends Application
{
	private Label headsLabel;
	private Label tailsLabel;
	private Image headsImage;
	private Image tailsImage;
	private ImageView headsIView;
	private ImageView tailsIView;
	
	private static Random generator = new Random();
	
	public static void main(String[] args)
	{
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		headsImage = new Image("image/Heads.jpg");
		tailsImage = new Image("image/Tails.jpg");
		
		headsIView = new ImageView(headsImage);
		tailsIView = new ImageView(tailsImage);
		
		Button flipButton = new Button("Flip a coin");
		flipButton.setOnAction(new flipButtonHandler());
		
		headsLabel = new Label("You flipped heads");
		tailsLabel = new Label("You flipped tails");
		
		HBox hbox1 = new HBox(10, headsLabel, headsIView);
		HBox hbox2 = new HBox(10, tailsLabel, tailsIView);
		
		VBox vbox = new VBox(10, hbox1, hbox2, flipButton);
		
		vbox.setAlignment(Pos.CENTER);
		vbox.setPadding(new Insets (10));
		
		tailsLabel.setVisible(false);
		headsLabel.setVisible(false);
		tailsIView.setVisible(false);
		headsIView.setVisible(false);
		
		Scene scene = new Scene(vbox);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Heads or Tails");
		primaryStage.show();
	}
	
	private class flipButtonHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			int result = generator.nextInt(2); /* Zero or One - two choices */
			if (result == 0)
			{
				headsLabel.setVisible(true);
				tailsLabel.setVisible(false);
				headsIView.setVisible(true);
				tailsIView.setVisible(false);
			}
			else if (result == 1)
			{
				headsLabel.setVisible(false);
				tailsLabel.setVisible(true);
				headsIView.setVisible(false);
				tailsIView.setVisible(true);
			}
		}
	}
}
