
public class Lawn 
{
	//instance fields
	private double width;
	private double length;

	
	//default constructor
	 public Lawn ()
	{
		width = 0;
		length = 0;
	}
	 
	//constructor
	 public Lawn (double width, double length)
	{
		this.width = width;
		this.length = length;
	}
	
	public double getlength () 
    { 
          return length; 
    } 

    public void setlength (double length)
    { 
          this.length = length; 
    }
    
    public double getwidth () 
    { 
          return width; 
    } 

    public void setwidth (double width)
    { 
          this.width = width; 
    }
    
    public double getSqFt()
    {
    	return length * width;
    }
    
}