import java.util.Scanner;
public class LawnDriver 
{

	public static void main (String args[]) 
	{

		double length1;
		double width1;
		double length2;
		double width2;
		int weeklyFee;
		int weeklyFee2;
		
		Scanner keyboard = new Scanner(System.in);
		

		System.out.println("Enter the length of the first lawn: ");
		length1 = keyboard.nextDouble();
		
		System.out.println("Enter the width of the first lawn: ");
		width1 = keyboard.nextDouble();
		
		System.out.println("Enter the length of the second lawn: ");
		length2 = keyboard.nextDouble();
		
		System.out.println("Enter the width of the second lawn: ");
		width2 = keyboard.nextDouble();
		
		//instance of lawn class
		Lawn firstLawn = new Lawn(width1, length1);
		
		//instance of the default constructor
		Lawn secondLawn = new Lawn();
		secondLawn.setwidth(width2);
		secondLawn.setlength(length2);
		
		double lotSize1 = firstLawn.getSqFt();
		double lotSize2 = secondLawn.getSqFt();
		

		if (lotSize1 < 400)
		{
			weeklyFee = 25;
		}
		else if (lotSize1 < 600)
		{
			weeklyFee = 35;
		}
		else
		{
			weeklyFee = 50;
		}
		
		if (lotSize2 < 400)
		{
			weeklyFee2 = 25;
		}
		
		else if (lotSize2 < 600)
		{
			weeklyFee2 = 35;
		}
		else
		{
			weeklyFee2 = 50;
		}
		

		System.out.println("The payment for the first lot of " + lotSize1 + " square feet is $"
			+ weeklyFee + " per week, or $" + (weeklyFee * 20) + " for a 20 week seasonal fee");
		
		System.out.println("The payment for the second lot of " + lotSize2 + " square feet is $"
			+ weeklyFee2 + " per week, or $" + (weeklyFee2 * 20) + " for a 20 week seasonal fee");
	}
}

