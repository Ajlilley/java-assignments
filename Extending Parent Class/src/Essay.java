public class Essay extends GradedActivity
{
    private double grammar;
    private double spelling;
    private double correctLength;
    private double content;
    
    public Essay()
    {
    	grammar = 0;
    	spelling = 0;
    	correctLength = 0;
    	content = 0;
    }

    public Essay(double gr, double sp, double len, double cnt)
    {
        grammar = gr;
        spelling = sp;
        correctLength = len;
        content = cnt;

    }
    
    public void setGrammar(double g)
    {
        grammar = g;
    }
    
    public void setSpelling(double s)
    {

        spelling = s;
    }
    
    public void setCorrectLength(double c)
    {
        correctLength = c;
    }
    
    public void setContent(double c)
    {
        content = c;
    }
    
    public double getGrammar()
    {
        return grammar;
    }
    
    public double getSpelling()
    {
        return spelling;
    }

    public double getCorrectLength()
    {
        return correctLength;
    }

    public double getContent()
    {
        return content;
    }

    public double getTotalGrade()
    {
        return grammar + spelling + correctLength + content;
    }

    public String toString()
    {
     String str = "Grammar: " + grammar + " pts.\nSpelling: " + spelling + " pts.\nLength: "
       + correctLength + " pts.\nContent: " + content + " pts.";
     return str;
    }
}
