public class EssayDriver 
{
	public static void main(String[] args)
	 {
	  Essay essay1 = new Essay(10, 15, 12, 25);
	  
	  essay1.setScore(essay1.getTotalGrade());
	  
	  System.out.println(essay1);
	  System.out.println("\nScore: " + essay1.getTotalGrade() +" pts.\nGrade: " + essay1.getScore() +"%");
	 }
}
