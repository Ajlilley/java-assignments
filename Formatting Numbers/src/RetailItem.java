public class RetailItem 
{
	private String description;		// description of item
	private int unitsOnHand;		// number of items
	private double retailPrice;		// price of item



	
	//set methods
	public void setDescription(String des)

	{
		description = des;

	}

	public void setUnitsOnHand(int unit)

	{
		unitsOnHand = unit;

	}

	public void setRetailPrice(double price)

	{
		retailPrice = price;

	}

	
	//get methods
	public String getDescription()

	{
		return description;

	}

	public int getUnitsOnHand()

	{
		return unitsOnHand;

	}

	public double getRetailPrice()

	{

		return retailPrice;

	}
}
