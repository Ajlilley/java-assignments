public class CompareStrings
{  
	public static void main(String args[])
	{  
		String compare1="Tom";  
		String compare2="Tom";  
		String compare3="Sam";  
		String compare4="Mike";  
		String compare5="Aaron";
		
		System.out.println(compare1.compareTo(compare2));
		System.out.println(compare1.compareTo(compare3));
		System.out.println(compare1.compareTo(compare4));
		System.out.println(compare1.compareTo(compare5));
		System.out.println();
		
		if (compare1.equals(compare2))
		{
			System.out.println(compare1 + " is lexicographically equal to " + compare2);
		}
		else
		{
			System.out.println(compare1 + " is not lexicographically equal to" + compare2);
		}
		
		if (compare1.equals(compare3))
		{
			System.out.println(compare1 + " is lexicographically equal to " + compare3);
		}
		else
		{
			System.out.println(compare1 + " is not lexicographically equal to " + compare3);
		}
		if (compare1.equals(compare4))
		{
			System.out.println(compare1 + " is lexicographically equal to " + compare4);
		}
		else
		{
			System.out.println(compare1 + " is not lexicographically equal to " + compare4);
		}
		if (compare1.equals(compare5))
		{
			System.out.println(compare1 + " is lexicographically equal to " + compare5);
		}
		else
		{
			System.out.println(compare1 + " is not lexicographically equal to " + compare5);
		}
	}
}  
