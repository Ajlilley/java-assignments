public class Countable 
{
	private static int count = 0;
	
	public Countable()
	{
		count++;
	}
	
	public int getCount()
	{
		return count;
	}
}
