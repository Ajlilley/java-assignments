import java.io.*;
import java.util.Scanner;
public class ChargeAccountModification 
{
	public static void main(String[] args) throws IOException
	{
		PrintWriter  chargeAccountModification = new  PrintWriter("ChargeAccountModificationFile.txt");
		
		Scanner sc = new Scanner(System.in);
        System.out.print("Please enter account number: ");
        int accountNum123 = sc.nextInt();

        // validation check
        if (ChargeAccountValidation.validation(accountNum123) == true) 
        {
        	chargeAccountModification.println("Valid account!");
        } 
        else 
        {
        	chargeAccountModification.println("Invalid account!");
        }
        
        chargeAccountModification.close();
	}
}
