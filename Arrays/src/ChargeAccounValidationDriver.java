import java.util.Scanner;

public class ChargeAccounValidationDriver 
{

	public static void main(String[] args) 
	{	
		Scanner sc = new Scanner(System.in);
        System.out.print("Please enter account number: ");
        int accountNum123 = sc.nextInt();

        // validation check
        if (ChargeAccountValidation.validation(accountNum123) == true) 
        {
                System.out.println("Valid account!");
        } 
        else 
        {
                System.out.println("Invalid account!");
        }
	}

}

