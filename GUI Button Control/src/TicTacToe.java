import java.lang.Math;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class TicTacToe extends Application
{
	private Image circleImage;
	private Image crossMarkImage;
	private ImageView circleIView;
	private ImageView crossMarkIView;
	private GridPane pane;
	
	//private static Random generator = new Random();
		
	public static void main(String[] args)
	{
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception
	{
		circleImage = new Image("image/Circle.jpg");
		crossMarkImage = new Image("image/CrossMark.jpg");
		
		circleIView = new ImageView(circleImage);
		crossMarkIView = new ImageView(crossMarkImage);
		
		Button gameButton = new Button("Play Game");
		gameButton.setOnAction(new gameButtonHandler());
		
		pane = new GridPane();
		pane.setAlignment(Pos.CENTER);
		pane.setHgap(5);
		pane.setVgap(5);
	
		HBox hbox1 = new HBox(300, pane);
		VBox vbox = new VBox(30, hbox1, gameButton);
		
		hbox1.setAlignment(Pos.CENTER);
		vbox.setAlignment(Pos.CENTER);
	
		Scene scene = new Scene(vbox, 500, 500);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("TicTacToe Game");
		primaryStage.show();
	}
	private class gameButtonHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			for (int i = 0; i < 3; ++i)
			{
				for (int x = 0; x < 3; ++x)
				{
					int generator = (int)(Math.random()*2);
					
					if (generator == 0)
					{
						pane.add(new ImageView(circleImage), x, i);
					}
					else if (generator == 1)
					{
						pane.add(new ImageView(crossMarkImage), x, i);
					}
				}
			}
		}
	}
}
