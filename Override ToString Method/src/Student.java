public class Student 
{
	private String studentname;
    private int studentage;
    Student(String name, int age)
    {
         this.studentname = name;
         this.studentage = age;
    }
    
    @Override
    public String toString() 
    {
       return "The student's name is: " + this.studentname + " & they are: " + this.studentage;
    }
}
