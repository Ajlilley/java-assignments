import java.util.Scanner;

public class MonthDriver
{
	public static void main(String[] args) throws InvalidMonthException
	{
	    Scanner sc = new Scanner(System.in);
	    
	    Integer numberOfMonth;
	    
	    Month monthChoice = new Month();
	
	    System.out.print("Enter a number of month: ");
	    numberOfMonth = sc.nextInt();
	    
	    if(numberOfMonth > 0 || numberOfMonth <13)
	    {
	    	throw new InvalidMonthException();
	    }
	    else
	    {
	    monthChoice.setMonth(numberOfMonth);
	
	    System.out.println(numberOfMonth + " is the month of " + monthChoice.toString());
	    }
	}
}
