public class InvalidMonthException extends Exception 
{

	public InvalidMonthException()
	{
		super("Invalid month number");
	}
}
