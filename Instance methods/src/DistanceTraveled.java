
public class DistanceTraveled 
{
	//instance field
	double speed;
	double time;
	
	//constructor
	DistanceTraveled (double speed, double time)
	{
		this.speed = speed;
		this.time = time;
	}

	public double getSpeed() 
	{
		return speed;
	}

	public void setSpeed(double speed) 
	{
		this.speed = speed;
	}

	public double getTime() 
	{
		return time;
	}

	public void setTime(double time) 
	{
		this.time = time;
	}
	
	public double calcDistance()
	{
		return speed * time;
	}
	
}
