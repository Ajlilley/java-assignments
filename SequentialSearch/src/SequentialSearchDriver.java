public class SequentialSearchDriver 
{
	public static void main(String[] args)
	{
		int result;
		
		int[] array = {10, 45, 23, 44, 56, 100, 19, 75, 17};
		
		result = SequentialSearch.sequentailSearch(array, 56);
		
		if (result == -1)
		{
			System.out.println("Your search could not find 56");
		}
		
		else
		{
			System.out.println("Your search found 56 on array element " + (result + 1));
		}
	}
}
