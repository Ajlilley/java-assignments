public class SequentialSearch 
{
	public static int sequentailSearch(int[] array, int value)
	{
		int loop;
		int element;
		boolean result;
		
		loop = 0;
		element = -1;
		result = false;
		
		while(!result && loop < array.length)
		{
			if (array[loop] == value)
			{
				result = true;
				element = loop;
			}
			
			loop++;
		}
		return element;
	}
}
