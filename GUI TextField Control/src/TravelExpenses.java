import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;


public class TravelExpenses extends Application
{
	private Label totalExpenseIncurredLabel;
	private Label totalallowableExpenseLabel;
	private Label excessPaymentLabel;
	private Label amountSavedLabel;
	
	private TextField NumOfDaysTextField;
	private TextField airFareTextField;
	private TextField carRentalFeeTextField;
	private TextField milesDrivenTextField;
	private TextField parkingFeeTextField;
	private TextField taxiChargeTextField;
	private TextField registrationFeesTextField;
	private TextField lodgingChargesTextField;
	
	public static void main(String[] args)
	{
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		Label NumOfDaysLabel = new Label("Number of days on the trip: ");
		Label airFareLabel = new Label("Amount of airfare, if any: ");
		Label carRentalFeeLabel = new Label("Amount of car rental fees, if any: ");
		Label milesDrivenLabel = new Label("Number of miles driven, if a private wehicle was used: ");
		Label parkingFeeLabel = new Label("Amount of parking fees, if any: ");
		Label taxiChargeLabel = new Label("Amount of taxi charges, if any: ");
		Label registrationFeesLabel = new Label("Conference or seminar registration fees, if any: ");
		Label lodgingChargesLabel = new Label("Lodging charges, per night: ");
		
		totalExpenseIncurredLabel = new Label();
		totalallowableExpenseLabel = new Label();
		excessPaymentLabel = new Label();
		amountSavedLabel = new Label();
		
		NumOfDaysTextField = new TextField("0");
		airFareTextField = new TextField("0");
		carRentalFeeTextField = new TextField("0");
		milesDrivenTextField = new TextField("0");
		parkingFeeTextField = new TextField("0");
		taxiChargeTextField = new TextField("0");
		registrationFeesTextField = new TextField("0");
		lodgingChargesTextField = new TextField("0");
		
		Button calcButton = new Button("Calculate expenses");
		calcButton.setOnAction(new calcButtonHandler());
		
		HBox hbox1 = new HBox(10, NumOfDaysLabel, NumOfDaysTextField);
		HBox hbox2 = new HBox(10, airFareLabel, airFareTextField);
		HBox hbox3 = new HBox(10, carRentalFeeLabel, carRentalFeeTextField);
		HBox hbox4 = new HBox(10, milesDrivenLabel, milesDrivenTextField);
		HBox hbox5 = new HBox(10, parkingFeeLabel, parkingFeeTextField);
		HBox hbox6 = new HBox(10, taxiChargeLabel, taxiChargeTextField);
		HBox hbox7 = new HBox(10, registrationFeesLabel, registrationFeesTextField);
		HBox hbox8 = new HBox(10, lodgingChargesLabel, lodgingChargesTextField);
		VBox vbox = new VBox(10, hbox1, hbox2, hbox3, hbox4, hbox5, hbox6,hbox7, hbox8, calcButton, totalExpenseIncurredLabel, totalallowableExpenseLabel, excessPaymentLabel, amountSavedLabel);
		
		Scene scene = new Scene(vbox);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Travel Expenses");
		primaryStage.show();
	}
	
	class calcButtonHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			double numOfDays = Double.parseDouble(NumOfDaysTextField.getText());
			double airFare = Double.parseDouble(airFareTextField.getText());
			double carRental = Double.parseDouble(carRentalFeeTextField.getText());
			double milesDriven = Double.parseDouble(milesDrivenTextField.getText());
			double parkingFee = Double.parseDouble(parkingFeeTextField.getText());
			double taxiCharge = Double.parseDouble(taxiChargeTextField.getText());
			double registrationFee = Double.parseDouble(registrationFeesTextField.getText());
			double lodgingCharges = Double.parseDouble(lodgingChargesTextField.getText());
			
			double expenseIncurred = ((47 * numOfDays)+ parkingFee + taxiCharge + lodgingCharges + (0.42 * milesDriven) + airFare + carRental + registrationFee);
			double expenseAllowed = ((47 * numOfDays)+ (20 * numOfDays) + (40 * numOfDays) + (195 * numOfDays) + (0.42 * milesDriven));
			double excessPayment = (((20 * numOfDays) - parkingFee) + ((40 * numOfDays) - taxiCharge) + ((195 * numOfDays) - lodgingCharges) + (0.42 * milesDriven) + airFare + carRental + registrationFee); 
			
			totalExpenseIncurredLabel.setText(String.format("Total expenses incurred: $%,.2f", expenseIncurred));
			totalallowableExpenseLabel.setText(String.format("Total allowable expenses: $%,.2f",expenseAllowed));
			if(expenseIncurred > expenseAllowed)
			{
			excessPaymentLabel.setText(String.format("Excess amount owed: $%,.2f",excessPayment));
			amountSavedLabel.setText(String.format("Amount of money saved: NONE"));
			}
			else if(expenseIncurred <= expenseAllowed)
			{
			excessPaymentLabel.setText(String.format("Excess amount owed: $0.00"));
			amountSavedLabel.setText(String.format("Amount of money saved: $%,.2f", expenseAllowed - expenseIncurred));
			}
		}
	}
}
