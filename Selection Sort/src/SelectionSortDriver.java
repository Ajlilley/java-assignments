public class SelectionSortDriver 
{
	public static void main(String[] args)
	{  
        int[] arr1 = {7,11,1,99,32,45,74,21};  
        System.out.println("Before Selection Sort");
        
        for(int i:arr1)
        {  
            System.out.print(i+" ");  
        }  
        System.out.println();  
          
        SelectionSort.selectionSort(arr1);
         
        System.out.println("After Selection Sort");  
        for(int i:arr1){  
            System.out.print(i+" ");  
        }  
    }  
}
