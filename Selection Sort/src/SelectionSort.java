public class SelectionSort 
{
	public static void selectionSort(int[] arrary)
	{  
        for (int i = 0; i < arrary.length - 1; i++)  
        {  
            int index = i;  
            for (int x = i + 1; x < arrary.length; x++)
            {  
                if (arrary[x] < arrary[index])
                {  
                    index = x; 
                }  
            }  
            int smallerNumber = arrary[index];   
            arrary[index] = arrary[i];  
            arrary[i] = smallerNumber;  
        }  
    }  
}
