import java.util.ArrayList;

public class StudentDriver 
{
	 public static void main(String [] args)
     {
          ArrayList<Student> student= new ArrayList<Student>();
          student.add(new Student("Charle", 16));
          student.add(new Student("Aaron", 17));
          student.add(new Student("Tom", 18));
          student.add(new Student("Jack",  19));
          student.add(new Student("Sarah", 20));
          
          for (Student fill: student)
          {
              System.out.println(fill);
          }
     }
}
