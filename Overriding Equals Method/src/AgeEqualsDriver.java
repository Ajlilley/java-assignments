public class AgeEqualsDriver 
{
	public static void main(String[] args) 
    {
    	AgeEquals ageGroup1 = new AgeEquals(19, 12);
    	AgeEquals ageGroup2 = new AgeEquals(19, 12);
    	
        if (ageGroup1.equals(ageGroup2)) 
        {
            System.out.println("Age Group 1 is equal to age group 2");
        }
        else 
        {
            System.out.println("Age Group 1 is not equal to age group 2");
        }
    }
}
