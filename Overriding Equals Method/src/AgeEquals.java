public class AgeEquals 
{
    private double age1, age2;    
     
    public AgeEquals(double age1, double age2) 
    {
        this.age1 = age1;
        this.age2 = age2;
    }
    
        @Override
        public boolean equals(Object obj) 
        {
      
            if (obj == this) 
            {
                return true;
            }

            if (!(obj instanceof AgeEquals)) 
            {
                return false;
            }
             
            AgeEquals age = (AgeEquals) obj;
            return Double.compare(age1, age.age1) == 0 && Double.compare(age2, age.age2) == 0;
        }
}
