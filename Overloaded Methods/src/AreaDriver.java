public class AreaDriver 
{
	public static void main(String[] args)
	{
		System.out.println(Area.area(3.5));
		
		System.out.println(Area.area(2.5, 12.35));
		
		System.out.println(Area.area(Math.PI, 3.5, 4.43));
	}
}
