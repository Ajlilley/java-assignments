public class Area 
{
	public static double area(double radius)
	{
		return Math.PI * Math.pow(radius,2.0);
	}
	
	public static double area(double width, double length)
	{
		return width * length;
	}
	
	public static double area(double pi, double radius, double height)
	{
		return pi * Math.pow(radius,2.0) * height;
	}
}