import javafx.scene.control.Label;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.event.ActionEvent;

public class Automotive extends Application
{
	private TextField partChargesTextField;
	private TextField laborHoursTextField;
	
	private Label resultLabel1;
	
	private CheckBox oilCheck;
	private CheckBox lubeCheck;
	private CheckBox RadiatorCheck;
	private CheckBox TransmissionCheck;
	private CheckBox inspectionCheck;
	private CheckBox mufflerCheck;
	private CheckBox tirerotationCheck;
	
	private double oilCost = 0;
	private double lubeCost = 0;
	private double RadiatorCost = 0;
	private double TransmissionCost = 0;
	private double inspectionCost = 0;
	private double mufflerCost = 0;
	private double tirerotationCost = 0;
	private double totalCost = 0;
	
	public static void main(String[] args) throws NumberFormatException
	{
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		Label partChargesLabel = new Label("Part Charges: ");
		Label laborHoursLabel = new Label("Hours of Labor: ");
		
		partChargesTextField = new TextField("0");
		laborHoursTextField =  new TextField("0");
		
		Button calcButton = new Button("Calculate Charges");
		calcButton.setOnAction(new calcButtonHandler());
		
		Button exitButton = new Button("Exit");
		exitButton.setOnAction(actionEvent -> System.exit(0));
		
		resultLabel1 = new Label();
		
		oilCheck = new CheckBox("Oil Change ($26.00)");
		lubeCheck = new CheckBox("Lube Job ($18.00)");
		RadiatorCheck = new CheckBox("Radiator Flush ($30.00)");
		TransmissionCheck = new CheckBox("Transmission Flush ($80.00)");
		inspectionCheck = new CheckBox("Inspection ($15.00)");
		mufflerCheck = new CheckBox("Muffler replacement ($100.00)");
		tirerotationCheck = new CheckBox("Tire Rotation ($20.00)");
		
		HBox hbox1 = new HBox(20, partChargesLabel, partChargesTextField);
		HBox hbox2 = new HBox(10, laborHoursLabel, laborHoursTextField);
		HBox hbox3 = new HBox(10, calcButton, exitButton);
		VBox vbox = new VBox(10, oilCheck, lubeCheck, RadiatorCheck, TransmissionCheck, 
				inspectionCheck, mufflerCheck, tirerotationCheck, hbox1, hbox2, hbox3, resultLabel1);
		
		hbox1.setAlignment(Pos.CENTER_LEFT);
		hbox2.setAlignment(Pos.CENTER_LEFT);
		hbox3.setAlignment(Pos.CENTER);
		vbox.setAlignment(Pos.CENTER_LEFT);
		calcButton.setAlignment(Pos.CENTER);
		exitButton.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(vbox, 300, 325);
		
		primaryStage.setScene(scene);
		primaryStage.setTitle("Ranken's Automotive Maintenance");
		primaryStage.setHeight(372);
		primaryStage.setWidth(270);
		primaryStage.show();
		
	}
	
	class calcButtonHandler implements EventHandler<ActionEvent>
	{
		@Override
		public void handle(ActionEvent event)
		{
			
			if(oilCheck.isSelected())
			{
				oilCost = 26;
			}
			if(lubeCheck.isSelected())
			{
				lubeCost = 18;
			}
			if(RadiatorCheck.isSelected())
			{
				RadiatorCost = 30;
			}
			if(TransmissionCheck.isSelected())
			{
				TransmissionCost = 80;
			}
			if(inspectionCheck.isSelected())
			{
				inspectionCost = 15;
			}
			if(mufflerCheck.isSelected())
			{
				mufflerCost = 100;
			}
			if(tirerotationCheck.isSelected())
			{
				tirerotationCost = 20;
			}
			
			try 
			{
				double laborInput = Double.parseDouble(laborHoursTextField.getText());
				double partsInput = Double.parseDouble(partChargesTextField.getText());
				
		        if (laborInput >= 0 && partsInput >=0) 
		        {
		        	totalCost = (oilCost + lubeCost + RadiatorCost + TransmissionCost + inspectionCost + mufflerCost 
							+ tirerotationCost + (laborInput * 20) + partsInput);
		        	resultLabel1.setText(String.format("Total Charges: $%,.2f", totalCost));

		        }
		    }
			catch (NumberFormatException e) 
			{
		            System.out.println("Please insert a number...");
		    }
		}
	}
}
